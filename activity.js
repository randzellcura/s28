// 3. insertOne method
db.hotel.insert({
    name: "Single",
    accomodates: 2,
    price: 1000,
    description: "A simple room with all the basic necessities.",
    rooms_available: 10,
    isAvalable: false
})

// 4. insertMany method

db.hotel.insertMany([
    {
        name: "Single",
        accomodates: 2,
        price: 1000,
        description: "A simple room with all the basic necessities.",
        rooms_available: 10,
        isAvalable: false
    },
    {
        name: "Double",
        accomodates: 3,
        price: 2000,
        description: "A room fit for a small family going on vacation.",
        rooms_available: 5,
        isAvalable: false
    },
    {
        name: "Queen",
        accomodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway.",
        rooms_available: 15,
        isAvalable: false
    }
])

// 5. find method

db.hotel.find({ name: "Double" })

// 6. updateOne method

db.hotel.updateOne(
    {
        name: "Queen",
    },
    {
        $set: {
            rooms_available: 0

        }
    }
)

// 7. deleteMany method

db.hotel.deleteMany ({})